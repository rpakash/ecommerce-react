import "./css/style.css";
import Header from "./Components/Header";
import Footer from "./Components/Footer";
import Container from "./Components/Container";
import { Route } from "react-router-dom";
import SingleProduct from "./Components/SingleProduct";
import React from "react";
import axiosInstance from "../src/utility/axios-instance";
import Cart from "./Components/Cart";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      loading: false,
      error: false,
      productToEdit: null,
      currentProductIndex: null,
      isChanged: false,
      successfullyUpdated: false,
      showWarningPopup: false,
      cart: new Map(),
    };
  }

  selectProductToEdit = (index) => {
    if (!this.state.isChanged) {
      this.setState((state) => ({
        productToEdit: state.products[index],
        currentProductIndex: index,
        successfullyUpdated: false,
      }));
    } else {
      this.setState({
        showWarningPopup: true,
        tempIndex: index,
      });
    }
  };

  addProductToCart = (product) => {
    if (this.state.cart.has(product)) {
      this.setState((state) => {
        let totalItems = state.cart.get(product);
        return {
          cart: state.cart.set(product, totalItems + 1),
        };
      });
    } else {
      this.setState((state) => ({
        cart: state.cart.set(product, 1),
      }));
    }
  };

  changeQuantity = (product, quantity) => {
    this.setState((state) => {
      return {
        cart: state.cart.set(product.id, quantity),
      };
    });
  };

  removeItemFromCart = (product) => {
    this.setState((state) => {
      state.cart.delete(product.id);
      return {
        cart: state.cart,
      };
    });
  };

  forceChangeProductInForm = () => {
    this.setState(
      {
        isChanged: false,
        showWarningPopup: false,
      },
      () => {
        this.selectProductToEdit(this.state.tempIndex);
      }
    );
  };

  handleFormSubmit = () => {
    this.setState((state) => {
      let products = state.products;
      products[state.currentProductIndex] = state.productToEdit;
      return { products, successfullyUpdated: true, isChanged: false };
    });
  };

  handleChange = (event) => {
    const name = event.target.name;

    this.setState((state) => ({
      isChanged: true,
      productToEdit: { ...state.productToEdit, [name]: event.target.value },
    }));
  };

  closePopup = () => {
    this.setState({
      showWarningPopup: false,
    });
  };
  componentDidMount() {
    this.setState(
      {
        loading: true,
      },
      () => {
        axiosInstance
          .get("/products/")
          .then((response) => {
            this.setState({
              products: response.data,
            });
          })
          .catch((error) => {
            this.setState({
              error: true,
            });
          })
          .finally(() => {
            this.setState({
              loading: false,
            });
          });
      }
    );
  }
  render() {
    return (
      <div className="App">
        <Header cartItems={this.state.cart.size}></Header>
        <Route
          path="/"
          exact
          render={(props) => (
            <Container
              {...props}
              loading={this.state.loading}
              products={this.state.products}
              error={this.state.error}
              showWarningPopup={this.state.showWarningPopup}
              selectProductToEdit={this.selectProductToEdit}
              closePopup={this.closePopup}
              forceChangeProductInForm={this.forceChangeProductInForm}
              productToEdit={this.state.productToEdit}
              handleChange={this.handleChange}
              handleFormSubmit={this.handleFormSubmit}
              isChanged={this.state.isChanged}
              isSuccess={this.state.successfullyUpdated}
            />
          )}
        />
        <Route
          path="/product/:id"
          render={(props) => (
            <SingleProduct
              {...props}
              addProductToCart={this.addProductToCart}
              cart={this.state.cart}
            />
          )}
        />
        <Route
          path="/cart"
          render={(props) => (
            <Cart
              {...props}
              cart={this.state.cart}
              changeQuantity={this.changeQuantity}
              removeItem={this.removeItemFromCart}
              products={this.state.products}
            />
          )}
        />
        <Footer></Footer>
      </div>
    );
  }
}

export default App;
