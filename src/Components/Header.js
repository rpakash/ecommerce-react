import Logo from "../images/dribbble-ar21.svg";
import { Link, NavLink } from "react-router-dom";
import HomeIcon from "../images/house-solid.svg";
import CartIcon from "../images/cart.svg";

function Header(props) {
  return (
    <section className="header-top">
      <Link to={"/"}>
        <img className="logo" src={Logo} alt="logo" />
      </Link>
      <div className="nav-menu">
        <div className="nav-left">
          <NavLink activeClassName="link-active" to={"/"} exact>
            Home
          </NavLink>
        </div>
        <div className="nav-right">
          <NavLink
            activeClassName="link-active"
            to={"/cart"}
            className="nav-cart"
          >
            Cart <span className="nav-cart">{props.cartItems}</span>
          </NavLink>
        </div>
      </div>
      <div className="mobile-nav">
        <NavLink activeClassName="link-active" to={"/"} exact>
          <img src={HomeIcon} alt="home" />
        </NavLink>
        <NavLink
          activeClassName="link-active"
          to={"/cart"}
          className="nav-cart"
        >
          <img src={CartIcon} alt="cart" />{" "}
        </NavLink>
      </div>
    </section>
  );
}

export default Header;
