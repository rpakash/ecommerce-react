import React, { Component } from "react";
import CartItem from "./CartItem";
import noCart from "../images/no-cart.png";
import Popup from "./Popup";

export class Cart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showWarning: false,
    };
  }
  handleRemoveItem = () => {
    this.props.removeItem(this.state.toRemove);
    this.setState({
      showWarning: false,
      toRemove: null,
    });
  };

  showPopup = (product) => {
    this.setState({
      showWarning: true,
      toRemove: product,
    });
  };

  render() {
    let productIds = [...this.props.cart.keys()];
    return (
      <>
        {this.state.showWarning && (
          <Popup className="popup">
            <p className="popup-title">
              Do you want to remove the item from the cart?
            </p>
            <button
              className="popup-button red-border"
              onClick={this.handleRemoveItem}
            >
              Remove
            </button>
            <button
              className="popup-button green-border"
              onClick={() => {
                this.setState({
                  showWarning: false,
                  toRemove: null,
                });
              }}
            >
              No
            </button>
          </Popup>
        )}
        <div className="cart-container">
          <div className="cart">
            <p className="cart-header">Cart Items</p>

            {this.props.cart.size > 0 &&
              this.props.products
                .filter((product) => {
                  return productIds.includes(product.id);
                })
                .map((item) => {
                  return (
                    <CartItem
                      key={item.id}
                      product={item}
                      quantity={this.props.cart.get(item.id)}
                      changeQuantity={this.props.changeQuantity}
                      removeItem={this.showPopup}
                    ></CartItem>
                  );
                })}
            {this.props.cart.size === 0 && (
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <img
                  src={noCart}
                  alt="no-items"
                  style={{ width: "96px", height: "96px" }}
                />
                <p style={{ fontSize: "1.5rem" }}>
                  No products in your cart. Add some products to your cart
                </p>
              </div>
            )}
          </div>
          {this.props.cart.size > 0 && (
            <div className="summary">
              <p className="summary-heading">Cart Summary</p>
              <div className="summary-info">
                <p>Total Items</p>
                <p>{this.props.cart.size}</p>
              </div>
              <div className="summary-info" style={{ fontWeight: "bold" }}>
                <p>Total Price</p>
                <p>
                  $
                  {this.props.products
                    .filter((product) => {
                      return productIds.includes(product.id);
                    })
                    .reduce((acc, item) => {
                      return (acc =
                        acc + this.props.cart.get(item.id) * item.price);
                    }, 0)
                    .toFixed(2)}
                </p>
              </div>
            </div>
          )}
        </div>
      </>
    );
  }
}

export default Cart;
