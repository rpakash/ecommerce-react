import star from "../images/star.svg";
import { Link } from "react-router-dom";

function Product(
  { id, image, title, price, description, rating },
  index,
  selectProduct
) {
  return (
    <div className="cards" key={id}>
      <img className="cards-image" src={image} alt="product" />
      <div className="cards-text">
        <div className="cards-title-outer">
          <Link to={`/product/${id}`}>
            <p className="cards-title">{title}</p>
          </Link>
        </div>
        <p className="cards-description">{description}</p>
        <div className="cards-info">
          <div>
            <p className="price-title">Price:</p>
            <p className="price">${price}</p>
          </div>
          <div className="rating-box">
            <img src={star} className="cards-rating" alt="star" />
            <p className="rating">
              {rating.rate} ({rating.count})
            </p>
          </div>
        </div>
        <button
          className="edit-product-btn"
          onClick={() => selectProduct(index)}
        >
          Edit Product
        </button>
      </div>
    </div>
  );
}

export default Product;
