import { NavLink } from "react-router-dom";

function Footer() {
  return (
    <footer className="footer-nav">
      <nav>
        <NavLink to={"/"} exact activeClassName="link-active">
          Home
        </NavLink>
      </nav>
      <p>&copy; Product Design</p>
    </footer>
  );
}

export default Footer;
