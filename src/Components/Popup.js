function Popup(props) {
  return (
    <div className="popupbg">
      <div className="popup">{props.children}</div>
    </div>
  );
}

export default Popup;
