import React, { Component } from "react";
import Product from "./Product";
export default class ProductsList extends Component {
  render() {
    return (
      <section id="products-all">
        <section id="product">
          {this.props.products.map((item, index) => {
            return Product(item, index, this.props.selectProduct);
          })}
        </section>
        {this.props.children}
      </section>
    );
  }
}
