import React, { Component } from "react";
import validator from "validator";

export class EditForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errors: {},
    };
  }

  checkFormErrors = (event) => {
    event.preventDefault();

    if (!this.props.isChanged) {
      return;
    }

    let target = event.target;
    let errorOccured = false;

    this.setState({
      errors: {},
    });

    if (validator.isEmpty(target.title.value.trim())) {
      this.setState((state) => ({ errors: { ...state.errors, title: true } }));
      errorOccured = true;
    }

    if (validator.isEmpty(target.description.value.trim())) {
      this.setState((state) => ({
        errors: { ...state.errors, description: true },
      }));
      errorOccured = true;
    }

    if (
      !validator.isFloat(target.price.value, {
        gt: 0,
      })
    ) {
      this.setState((state) => ({ errors: { ...state.errors, price: true } }));
      errorOccured = true;
    }

    if (validator.isEmpty(target.category.value.trim())) {
      this.setState((state) => ({
        errors: { ...state.errors, category: true },
      }));
      errorOccured = true;
    }

    if (
      !validator.isURL(target.image.value, {
        protocols: ["http", "https"],
        allow_underscores: true,
        require_protocol: true,
      })
    ) {
      this.setState((state) => ({
        errors: { ...state.errors, image: true },
      }));
      errorOccured = true;
    }

    if (!errorOccured) {
      this.props.handleFormSubmit();
    }
  };

  componentDidUpdate(prevProps) {
    if (this.props.product?.id !== prevProps.product?.id) {
      this.setState({
        errors: {},
      });
    }
  }

  render() {
    let title = this.props.product?.title;
    let description = this.props.product?.description;
    let price = this.props.product?.price;
    let category = this.props.product?.category;
    let imageUrl = this.props.product?.image;

    if (this.props.product) {
      return (
        <div className="edit-section">
          <h2>Edit Product</h2>
          <form className="edit-form" onSubmit={this.checkFormErrors}>
            <label htmlFor="product-title">Title</label>
            <div>
              <input
                value={title}
                name="title"
                placeholder="Product Title"
                id="product-title"
                onChange={this.props.handleChange}
                className={this.state.errors.title && "error-message"}
              ></input>
              {this.state.errors.title && (
                <p className="field-error">Title is requried</p>
              )}
            </div>
            <label htmlFor="product-description">Description</label>
            <div>
              <textarea
                rows="5"
                htmlFor="product-description"
                value={description}
                name="description"
                placeholder="Product Description"
                onChange={this.props.handleChange}
                className={this.state.errors.description && "error-message"}
              ></textarea>
              {this.state.errors.description && (
                <p className="field-error">Description is requried</p>
              )}
            </div>
            <label htmlFor="product-image">Image Url</label>
            <div>
              <input
                value={imageUrl}
                name="image"
                placeholder="Product Category"
                id="product-category"
                onChange={this.props.handleChange}
                className={this.state.errors.image && "error-message"}
              ></input>
              {this.state.errors.image && (
                <p className="field-error">Vaild image url is requried</p>
              )}
            </div>
            <label htmlFor="product-category">Category</label>
            <div>
              <input
                value={category}
                name="category"
                placeholder="Product Category"
                id="product-category"
                onChange={this.props.handleChange}
                className={this.state.errors.category && "error-message"}
              ></input>
              {this.state.errors.category && (
                <p className="field-error">Category is requried</p>
              )}
            </div>
            <label htmlFor="product-price">Price</label>
            <div>
              <input
                id="product-price"
                type="number"
                value={price}
                placeholder="Product Price"
                name="price"
                onChange={this.props.handleChange}
                className={this.state.errors.price && "error-message"}
              ></input>
              {this.state.errors.price && (
                <p className="field-error">
                  Price is requried and should be a greater than zero
                </p>
              )}
            </div>
            <div>
              <button
                className={
                  this.props.isChanged ? "submit-button" : "disabled-button"
                }
              >
                Update Product
              </button>
            </div>
            {this.props.isSuccess && !this.props.isChanged && (
              <p className="success-message">Product successfully updated</p>
            )}
          </form>
        </div>
      );
    } else {
      return (
        <div className="edit-section">
          <p
            style={{
              textAlign: "center",
              display: "block",
              fontWeight: "bold",
            }}
            className="edit-form"
          >
            Select Product to edit
          </p>
        </div>
      );
    }
  }
}

export default EditForm;
