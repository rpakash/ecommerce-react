import React, { Component } from "react";
import axiosInstance from "../utility/axios-instance";
import star from "../images/star.svg";
import Loader from "./Loader";
import Notification from "./Notification";
import { Link } from "react-router-dom";

export class SingleProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      product: {},
      loading: false,
      error: false,
      showNotification: false,
      message: "",
      showAddToCart: true,
    };
  }

  handleclick = () => {
    if (this.props.cart.has(this.state.product.id)) {
      this.setState({
        showNotification: true,
        message:
          "Product is already in cart and we have increased the quantity by 1",
        showAddToCart: false,
      });
    } else {
      this.setState({
        showNotification: true,
        message: "Product added to the cart",
        showAddToCart: false,
      });
    }

    this.props.addProductToCart(this.state.product.id);
  };

  closeNotification = () => {
    this.setState({
      showNotification: false,
      message: "",
    });
  };

  componentDidMount() {
    this.setState(
      {
        loading: true,
      },
      () => {
        axiosInstance(`/products/${this.props.match.params.id}`)
          .then((response) => {
            this.setState({
              product: response.data,
            });
          })
          .catch((error) => {
            this.setState({
              error: true,
            });
            console.error(error);
          })
          .finally(() => {
            this.setState({
              loading: false,
            });
          });
      }
    );
  }
  render() {
    return (
      <>
        <section id="single-product">
          <div className="product-container">
            {this.state.showNotification && (
              <Notification
                message={this.state.message}
                close={this.closeNotification}
              ></Notification>
            )}
            {!this.state.error && !this.state.loading && (
              <>
                <img
                  className="product-image"
                  src={this.state.product.image}
                  alt="product"
                />
                <div className="product-info">
                  <p className="product-category">
                    {this.state.product.category}
                  </p>
                  <h2>{this.state.product.title}</h2>
                  <div className="rating-box">
                    <img src={star} className="cards-rating" alt="star" />
                    <p className="rating">
                      {this.state.product.rating?.rate} (
                      {this.state.product.rating?.count})
                    </p>
                  </div>
                  <p className="product-price">${this.state.product.price}</p>
                  <p className="product-description">
                    {this.state.product.description}
                  </p>
                  {this.state.showAddToCart && (
                    <button className="addToCart" onClick={this.handleclick}>
                      Add to cart
                    </button>
                  )}
                  {!this.state.showAddToCart && (
                    <Link to={"/cart"}>
                      <button className="cart-btn">Go to Cart</button>
                    </Link>
                  )}
                </div>
              </>
            )}
            {this.state.error && !this.state.loading && (
              <p
                style={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  height: "100%",
                  width: "100%",
                  fontWeight: "bold",
                }}
              >
                An error occured while fetching the product details.
              </p>
            )}
            {this.state.loading && <Loader />}
          </div>
        </section>
      </>
    );
  }
}

export default SingleProduct;
