import React, { Component } from "react";
import EditForm from "./EditForm";
import ProductsList from "./ProductsList";
import Loader from "./Loader";
import Popup from "./Popup";

export class Container extends Component {
  render() {
    return (
      <>
        {!this.props.loading &&
          !this.props.error &&
          this.props.products.length === 0 && (
            <p
              className="error"
              style={{ textAlign: "center", fontWeight: "bolder" }}
            >
              No Products found
            </p>
          )}
        {this.props.loading && <Loader />}
        {this.props.error && (
          <p
            className="error"
            style={{ textAlign: "center", fontWeight: "bolder" }}
          >
            An error occured while fetching the products
          </p>
        )}
        {!this.props.loading &&
          !this.props.error &&
          this.props.products.length > 0 && (
            <div className="container">
              <ProductsList
                products={this.props.products}
                selectProduct={this.props.selectProductToEdit}
              >
                {this.props.showWarningPopup && (
                  <Popup>
                    <p className="popup-title">
                      There is a unsaved changes. Do you want to discard the
                      changes?
                    </p>
                    <button
                      className="popup-button red-border"
                      onClick={this.props.forceChangeProductInForm}
                    >
                      Discard
                    </button>
                    <button
                      className="popup-button green-border"
                      onClick={this.props.closePopup}
                    >
                      No
                    </button>
                  </Popup>
                )}
              </ProductsList>
              <EditForm
                product={this.props.productToEdit}
                handleChange={this.props.handleChange}
                handleFormSubmit={this.props.handleFormSubmit}
                isChanged={this.props.isChanged}
                isSuccess={this.props.successfullyUpdated}
              ></EditForm>
            </div>
          )}
      </>
    );
  }
}

export default Container;
