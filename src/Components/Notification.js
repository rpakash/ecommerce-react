import React from "react";

class Notification extends React.Component {
  componentDidMount = () => {
    setTimeout(() => {
      this.props.close();
    }, 3000);
  };
  render() {
    return (
      <div className="notification elementToFadeInAndOut">
        <p>{this.props.message}</p>
      </div>
    );
  }
}

export default Notification;
