import React, { Component } from "react";

export class CartItem extends Component {
  constructor(props) {
    super(props);
    this.quantity = React.createRef();
  }

  handleInputChange = (product) => {
    if (
      !Number(this.quantity.current.value) ||
      this.quantity.current.value > 100
    ) {
      return;
    }
    this.props.changeQuantity(product, this.quantity.current.value);
  };

  handleButtonClick = (product, type) => {
    let currentQuantity = this.quantity.current.value;
    if (type === "increase") {
      currentQuantity = +currentQuantity + 1;
    } else {
      currentQuantity = +currentQuantity - 1;
    }
    if (currentQuantity < 1 || this.quantity.current.value > 100) {
      return;
    }
    this.props.changeQuantity(product, currentQuantity);
  };

  render() {
    const quantity = this.props.quantity;

    return (
      <>
        <hr className="cart-break" />
        <div className="cart-item" key={this.props.product.id}>
          <img
            src={this.props.product.image}
            alt="product"
            className="cart-item-image"
          ></img>
          <div className="cart-item-info">
            <div>
              <p className="cart-item-title">{this.props.product.title}</p>
              <p className="cart-item-price">${this.props.product.price}</p>
            </div>
            <div className="cart-item-quantity">
              <p>Quantity</p>
              <div className="quantity-options">
                <button
                  onClick={() => {
                    this.handleButtonClick(this.props.product, "increase");
                  }}
                >
                  +
                </button>
                <input
                  value={quantity}
                  onChange={() => {
                    this.handleInputChange(this.props.product);
                  }}
                  placeholder="quantity"
                  className="quantity-input"
                  ref={this.quantity}
                />
                <button
                  onClick={() => {
                    this.handleButtonClick(this.props.product, "decrease");
                  }}
                >
                  -
                </button>
              </div>
              <div>
                <button
                  className="remove-item"
                  onClick={() => {
                    this.props.removeItem(this.props.product);
                  }}
                >
                  REMOVE
                </button>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default CartItem;
